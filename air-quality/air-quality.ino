#include <Arduino.h>
#include "NTPClient.h"

#include "air-pollution-monitor.h"
#include "config.h"
#include "credentials.h"
#include "gsheets.h"
#include "slack.h"
#include "urlencode.h"
#include "wifi.h"


// On Arduino Wifi Rev2:
//     Serial1: Pins 0/1
//     Serial:  USB Output

unsigned long prevEvent = (millis() < CFG_SAMPLING_FREQUENCY_MS) ? 0 : millis() - CFG_SAMPLING_FREQUENCY_MS;

AirPollutionMonitor g_monitor;
ArduinoUnoWifi      g_wifi;
WiFiUDP             g_ntpUDP;
NTPClient           g_timeClient(g_ntpUDP, "pool.ntp.org");
SlackClient         g_sclient;
GoogleSheetsClient  g_gclient;

void setup()
{
    Serial.begin(9600);
    Serial.setTimeout(1500);

    g_monitor.setup();
    g_wifi.setup();
}

void loop()
{
    // Get Air Pollution levels for 2.5 and 10.0
    // In units of ug/m3
    int pm[2] = { -1, -1 };

#if 0 // keep for debugging
    char fbuf[256];
    sprintf(fbuf, "Now: %lu, PrevEvent: %lu, Waiting till: %lu", millis(), prevEvent, prevEvent + CFG_SAMPLING_FREQUENCY_MS);
    Serial.println(fbuf);
#endif

    // TODO: Handle overflow
    if (prevEvent == 0 || millis() > prevEvent + CFG_SAMPLING_FREQUENCY_MS)
    {
        g_monitor.getPMValues(pm, 2);

        if (pm[0] != -1 && pm[1] != -1)
        {
            Serial.print("PM2.5:");
            Serial.print(pm[0]);
            Serial.print(" PM10:");
            Serial.print(pm[1]);
            Serial.println();

            if (g_wifi.connect())
            {
                g_timeClient.begin();
                g_timeClient.update();

                /*
                {
                    "datetime" : "2019-08-12T13:45:55"
                    "location" : "xyz",
                    "pm25" : "xyz",
                    "pm100": "xyz",
                    "version": "1.0"
                }
                */
                String datetime = g_timeClient.getFormattedDate();
                String strJson = String("{") + \
                    "\"datetime\": \"" + datetime + "\"," + \
                    "\"location\": \"" + CFG_LOCATION + "\"," + \
                    "\"pm25\": \"" + pm[0] + "\"," + \
                    "\"pm100\": \"" + pm[1] + "\"," + \
                    "\"version\": \"1.0\"" + \
                    "}";
                String strData = String("") + \
                    "datetime=" + urlencode(datetime) + "&" + \
                    "location=" + urlencode(CFG_LOCATION) + "&" + \
                    "pm25=" + pm[0] + "&" + \
                    "pm100=" + pm[1] + "&" + \
                    "version=" + "1.0";

#if CFG_USE_SLACK == 1
                // Publish to Slack
                // Maybe should only publish to slack Errors and when it goes above/below a threshold
                g_sclient.postMessage(strJson);
#endif // #if CFG_USE_SLACK == 1

#if CFG_USE_GOOGLE_SHEETS == 1
                g_gclient.postMessage(strData);
#endif // #if CFG_USE_GOOGLE_SHEETS == 1

                // Disconnect from wifi, first stop the NTP client
                g_timeClient.end();
                g_wifi.disconnect();
            }
            else
            {
                Serial.println("Failed to connect to any wifi network");
            }

            char fbuf[256];
            sprintf(fbuf, "Waiting for (ms): %lu", CFG_SAMPLING_FREQUENCY_MS);
            Serial.println(fbuf);

            // Only wait if we successfully received valid measurements from the air monitor
            // b/c after waiting a minute, the 'first' request will not return a valid value
            prevEvent = millis();
        }
        else
        {
            Serial.println("Failed to get measurements from the device");
        }

    }
}
