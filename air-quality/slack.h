#ifndef SLACK_H__
#define SLACK_H__

#include <SPI.h>
#include <WiFiNINA.h>

#include "config.h"
#include "credentials.h"


//////////////////////////////////////////////////////////////////////////////
//! @class SlackClient
//! @brief Client for communicating with Slack
//////////////////////////////////////////////////////////////////////////////
class SlackClient
{
public:
    //////////////////////////////////////////////////////////////////////////
    //! Constructor
    SlackClient()
    {
        m_strSlackHost = "hooks.slack.com";
    }

    //////////////////////////////////////////////////////////////////////////
    //! Destructor
    ~SlackClient() {}

    //////////////////////////////////////////////////////////////////////////
    //! Post a message to slack
    //! @param[in] rawData The message to be posted to Slack
    void postMessage(String rawData);

private:
    char *m_strSlackHost;
    WiFiSSLClient m_client;
};


//////////////////////////////////////////////////////////////////////////////
void SlackClient::postMessage(String rawData)
{
    // Replace any " with \"
    rawData.replace("\"", "\\\"");

    String postData = String("{\"text\":\"" + rawData + "\"}");

    Serial.println("Posting to slack: " + postData);

    if (m_client.connect(m_strSlackHost, 443))
    {
        Serial.println("Connected to: " + String(m_strSlackHost));
        // Make a HTTP request:
        m_client.println("POST " + String(slackURLPath) + " HTTP/1.1");
        m_client.print("Host: ");
        m_client.println(m_strSlackHost);
        m_client.println("Content-type: application/json");
        m_client.println("Connection: close");
        m_client.print("Content-Length: ");
        m_client.println(postData.length());
        m_client.println();
        m_client.println(postData);

        String result = m_client.readStringUntil('\n');
        Serial.println(result);
        if (result.startsWith("HTTP/1.1 200 OK"))
        {
            Serial.println("Successfully sent to Slack!");
        }
        else // if it failed, print the rest of the message
        {
            while (m_client.available())
            {
                char c = m_client.read();
                Serial.write(c);
            }
        }

        // Disconnect
        m_client.stop();
    }
    else
    {
        Serial.println("Failed to connect to: " + String(m_strSlackHost));
    }

}

#endif // #ifndef SLACK_H__
