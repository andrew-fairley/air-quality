#ifndef CONFIG_H__
#define CONFIG_H__

#define CFG_USE_DEBUG true

// Sampling frequency in ms
// 360000 = 6 minutes
// 900000 = 15 minutes
#define CFG_SAMPLING_FREQUENCY_MS 900000UL


// According to:
// https://www.epa.gov/sites/production/files/2016-04/documents/2012_aqi_factsheet.pdf
// The new standard is less than 12 micrograms per cubic meter
#define CFG_POLLUTION_THRESHOLD_PS25_HIGH   12

// Label to identify measurements when posted to Slash or Power BI
//#define CFG_LOCATION                        "CONDO_INDOOR"
#define CFG_LOCATION                        "HOME_INDOOR"

#define CFG_USE_SLACK                       0
#define CFG_SLACK_CHANNEL_NAME              "kirkland-ap"

#define CFG_USE_GOOGLE_SHEETS               1

#endif // #ifndef CONFIG_H__
