#ifndef GSHEETS_H__
#define GSHEETS_H__

#include <SPI.h>
#include <WiFiNINA.h>

#include "config.h"
#include "credentials.h"
#include "urlencode.h"


//////////////////////////////////////////////////////////////////////////////
//! @class GoogleSheetsClient
//! @brief Client for communicating with Google Sheets
//////////////////////////////////////////////////////////////////////////////
class GoogleSheetsClient
{
public:
    //////////////////////////////////////////////////////////////////////////
    //! Constructor
    GoogleSheetsClient()
    {
        m_strGSheetsHost = "script.google.com";
    }

    //////////////////////////////////////////////////////////////////////////
    //! Destructor
    ~GoogleSheetsClient() {}

    //////////////////////////////////////////////////////////////////////////
    //! Post a message to Google Sheets
    //! @param[in] rawData Msg to be posted to Google Sheets format: "k0=v0&k1=v1"
    void postMessage(String rawData);

private:
    char *m_strGSheetsHost;
    WiFiSSLClient m_client;
};

//////////////////////////////////////////////////////////////////////////////
void GoogleSheetsClient::postMessage(String rawData)
{
    //String postData = urlencode(rawData);
    String postData = rawData;

    Serial.println("Posting to Google Sheets: " + postData);

    if (m_client.connect(m_strGSheetsHost, 443))
    {
        Serial.println("Connected to: " + String(m_strGSheetsHost));
        // Make a HTTP request:
        m_client.println("POST " + String(gsheetsURLPath) + " HTTP/1.1");
        m_client.print("Host: ");
        m_client.println(m_strGSheetsHost);
        m_client.println("Content-type: application/x-www-form-urlencoded");
        m_client.println("Connection: close");
        m_client.print("Content-Length: ");
        m_client.println(postData.length());
        m_client.println();
        m_client.println(postData);

        String result = m_client.readStringUntil('\n');
        Serial.println(result);
        if (result.startsWith("HTTP/1.1 200 OK"))
        {
            Serial.println("Successfully sent to Google Sheets!");
        }
        else // if it failed, print the rest of the message
        {

            while (m_client.available())
            {
                char c = m_client.read();
                Serial.write(c);
            }
        }

        // Disconnect
        m_client.stop();
    }
    else
    {
        Serial.println("Failed to connect to: " + String(m_strGSheetsHost));
    }
}

#endif
