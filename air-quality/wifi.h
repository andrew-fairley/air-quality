#ifndef WIFI_H__
#define WIFI_H__

#include <SPI.h>
#include <WiFiNINA.h>
// The following isn't included by WiFiNINA.h and needs to be included -after- WiFiNINA.h
#include <WiFiUdp.h>

#include "config.h"
#include "credentials.h"

#define SSID_AND_PW_LEN 256


//////////////////////////////////////////////////////////////////////////////
//! @class ArduinoUnoWifi
//! @brief Wifi connectivity for Arduino Uno Wifi Rev2
//////////////////////////////////////////////////////////////////////////////
class ArduinoUnoWifi
{
public:
    //////////////////////////////////////////////////////////////////////////
    //! Constructor
    ArduinoUnoWifi() {}

    //////////////////////////////////////////////////////////////////////////
    //! Destructor
    ~ArduinoUnoWifi() {}

    //////////////////////////////////////////////////////////////////////////
    //! Set up wifi
    //! Checks that the device has Wifi capability
    //! Prints out info about available networks
    //! Also checks that the firmware is up-to-date
    void setup();

    //////////////////////////////////////////////////////////////////////////
    //! Connects to wifi
    //! @return True on wifi connection success
    bool connect();

    //////////////////////////////////////////////////////////////////////////
    //! Disconnect from wifi
    void disconnect();

    //////////////////////////////////////////////////////////////////////////
    //! Print the available networks to Serial
    void listNetworks();

    //////////////////////////////////////////////////////////////////////////
    //! Display information about the currently connected network
    void printCurrentNet();

    //////////////////////////////////////////////////////////////////////////
    //! Print the IP address and MAC address
    void printWifiData();

    //////////////////////////////////////////////////////////////////////////
    //! Print the MAC address
    void printMacAddress(byte mac[]);

    //////////////////////////////////////////////////////////////////////////
    //! Helper to print the current wifi status
    //! @param[in] status The status
    //! @return String representation of the status
    String statusToString(int status);

    //////////////////////////////////////////////////////////////////////////
    //! Helper to convert the encryption type to a String
    //! @param[in] type The encryption type
    //! @return String representation of the type
    String encryptionTypeToString(int type);
};


//////////////////////////////////////////////////////////////////////////////
void ArduinoUnoWifi::setup()
{
    // check for the WiFi module:
    if (WiFi.status() == WL_NO_MODULE)
    {
        Serial.println("Communication with WiFi module failed!");
        // don't continue
        while (true);
    }

    String fv = WiFi.firmwareVersion();
    Serial.print("Firmware version:");
    Serial.println(fv);
    if (fv < WIFI_FIRMWARE_LATEST_VERSION)
    {
        Serial.println("Please upgrade the firmware");
        Serial.print("Latest version is: ");
        Serial.println(WIFI_FIRMWARE_LATEST_VERSION);
    }

    // print your MAC address:
    byte mac[6];
    WiFi.macAddress(mac);
    Serial.print("MAC: ");
    printMacAddress(mac);

    listNetworks();
}


//////////////////////////////////////////////////////////////////////////////
// Return true if it is able to connect
bool ArduinoUnoWifi::connect()
{
    int status = WL_IDLE_STATUS;

    int numSsid = WiFi.scanNetworks();
    if (numSsid == -1)
    {
        Serial.println("Couldn't get a wifi connection");
        while (true);
    }

    int num_credentials = sizeof(wifiCredentials) / sizeof(wifiCredentials)[0];

    // print the network number and name for each network found:
    for (int thisNet = 0; status != WL_CONNECTED && thisNet < numSsid; thisNet++)
    {
        String ssid = WiFi.SSID(thisNet);

        Serial.println("Network: " + ssid + ": Looking for credentials");

        //  Iterate through wifiCredentials
        for (int idx=0; status != WL_CONNECTED && idx < num_credentials; ++idx)
        {
            Serial.println("    Credentials for:" + wifiCredentials[idx][0]);

            // If we have credentials for this network...
            if (wifiCredentials[idx][0].equals(ssid))
            {
                String pass = wifiCredentials[idx][1];
                char cSSID[SSID_AND_PW_LEN];
                char cPass[SSID_AND_PW_LEN];
                ssid.toCharArray(cSSID, SSID_AND_PW_LEN);
                pass.toCharArray(cPass, SSID_AND_PW_LEN);

                // Try to connect
                switch (WiFi.encryptionType(thisNet))
                {
                    case ENC_TYPE_CCMP: // WPA2
                        status = WiFi.begin(cSSID, cPass);
                        break;
                    default:
                        // Skip the network
                        break;
                }

                Serial.println("    Connect status: " + statusToString(status));
            }
        }
    }

    if (status == WL_CONNECTED)
    {
        // Wait 10 seconds for connection
        delay(10000);

        printCurrentNet();
        printWifiData();
    }

    return (status == WL_CONNECTED);
}


//////////////////////////////////////////////////////////////////////////////
// Disconnects from wifi
void ArduinoUnoWifi::disconnect()
{
    Serial.println("Disconnecting from wifi");

    if (WiFi.status() == WL_CONNECTED)
    {
        WiFi.disconnect();
    }
}


//////////////////////////////////////////////////////////////////////////////
void ArduinoUnoWifi::printWifiData()
{
    // print your board's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);
    Serial.println(ip);

    // print your MAC address:
    byte mac[6];
    WiFi.macAddress(mac);
    Serial.print("MAC address: ");
    printMacAddress(mac);
}


//////////////////////////////////////////////////////////////////////////////
void ArduinoUnoWifi::printCurrentNet()
{
    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print the MAC address of the router you're attached to:
    byte bssid[6];
    WiFi.BSSID(bssid);
    Serial.print("BSSID: ");
    printMacAddress(bssid);

    // print the received signal strength:
    long rssi = WiFi.RSSI();
    Serial.print("signal strength (RSSI):");
    Serial.println(rssi);

    // print the encryption type:
    byte encryption = WiFi.encryptionType();
    Serial.print("Encryption Type:");
    Serial.println(encryptionTypeToString(encryption));
    Serial.println();
}


//////////////////////////////////////////////////////////////////////////////
// Print out the available networks
void ArduinoUnoWifi::listNetworks()
{
    // scan for nearby networks:
    Serial.println("** Scan Networks **");
    int numSsid = WiFi.scanNetworks();
    if (numSsid == -1)
    {
        Serial.println("Couldn't get a wifi connection");
        while (true);
    }

    // print the list of networks seen:
    Serial.print("number of available networks:");
    Serial.println(numSsid);

    // print the network number and name for each network found:
    for (int thisNet = 0; thisNet < numSsid; thisNet++)
    {
        Serial.print(thisNet);
        Serial.print(") ");
        Serial.print(WiFi.SSID(thisNet));
        Serial.print("\tSignal: ");
        Serial.print(WiFi.RSSI(thisNet));
        Serial.print(" dBm");
        Serial.print("\tEncryption: ");
        Serial.println(encryptionTypeToString(WiFi.encryptionType(thisNet)));
    }
}


//////////////////////////////////////////////////////////////////////////////
String ArduinoUnoWifi::encryptionTypeToString(int type)
{
    String sType = "unknown";

    switch (type)
    {
        case ENC_TYPE_WEP:      sType = "WEP"; break;
        case ENC_TYPE_TKIP:     sType = "WPA"; break;
        case ENC_TYPE_CCMP:     sType = "WPA2"; break;
        case ENC_TYPE_NONE:     sType = "none"; break;
        case ENC_TYPE_AUTO:     sType = "Auto"; break;
        case ENC_TYPE_UNKNOWN:  sType = "ENC_TYPE_UNKNOWN"; break;
        default:                sType = "unknown"; break;
    }

    return sType;
}


//////////////////////////////////////////////////////////////////////////////
String ArduinoUnoWifi::statusToString(int status)
{
    String sStatus = "unknown";

    switch (status)
    {
        case WL_CONNECTED:      sStatus = "WL_CONNECTED"; break;
        case WL_NO_SHIELD:      sStatus = "WL_NO_SHIELD"; break;
        case WL_IDLE_STATUS:    sStatus = "WL_IDLE_STATUS"; break;
        case WL_NO_SSID_AVAIL:  sStatus = "WL_NO_SSID_AVAIL"; break;
        case WL_SCAN_COMPLETED: sStatus = "WL_SCAN_COMPLETED"; break;
        case WL_CONNECT_FAILED: sStatus = "WL_CONNECT_FAILED"; break;
        case WL_CONNECTION_LOST:sStatus = "WL_CONNECTION_LOST"; break;
        case WL_DISCONNECTED:   sStatus = "WL_DISCONNECTED"; break;
        default:                sStatus = "unknown"; break;
    }

    return sStatus;
}

//////////////////////////////////////////////////////////////////////////////
void ArduinoUnoWifi::printMacAddress(byte mac[])
{
    for (int i = 5; i >= 0; i--)
    {
        if (mac[i] < 16)
        {
        Serial.print("0");
        }
        Serial.print(mac[i], HEX);
        if (i > 0)
        {
        Serial.print(":");
        }
    }
    Serial.println();
}


#endif // #ifndef WIFI_H__
