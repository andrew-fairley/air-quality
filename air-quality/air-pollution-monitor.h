#ifndef AIR_POLLUTION_MONITOR_H__
#define AIR_POLLUTION_MONITOR_H__

#include "config.h"

#define SEN0177_DATA_LEN 31 //0x42 + 31 bytes equal to 32 bytes

//////////////////////////////////////////////////////////////////////////////
//! @class AirPollutionMonitor
//! @brief Air pollution monitor (SEN0177)
//////////////////////////////////////////////////////////////////////////////
class AirPollutionMonitor
{
public:
    //////////////////////////////////////////////////////////////////////////
    //! Constructor
    AirPollutionMonitor() {}
    //////////////////////////////////////////////////////////////////////////
    //! Destructor
    ~AirPollutionMonitor() {}

    //////////////////////////////////////////////////////////////////////////
    //! Set up for receiving data from the device
    void setup();

    //////////////////////////////////////////////////////////////////////////
    //! Get the pollution levels
    //! @return array[] = { PM 2.5 value, PM 10 value }
    void getPMValues(int *pmvalues, int len);

private:
    //////////////////////////////////////////////////////////////////////////
    //! Checks the value from the monitor
    char checkValue(unsigned char *buf, char len);

    //////////////////////////////////////////////////////////////////////////
    //! Reading for PM 1.0 - not possible with SEN0177 but data format contains it
    int getPM10(unsigned char *buf);

    //////////////////////////////////////////////////////////////////////////
    //! Reading for PM 2.5
    int getPM25(unsigned char *buf);

    //////////////////////////////////////////////////////////////////////////
    //! Reading for PM 10
    int getPM100(unsigned char *buf);
};

/////////////////////////////////////////////////////////////////////////////
void AirPollutionMonitor::setup()
{
    // On Arduino Uno Wifi Rev2, Serial1 is pins 0/1
    Serial1.begin(9600);
    Serial1.setTimeout(1500);
}


/////////////////////////////////////////////////////////////////////////////
void AirPollutionMonitor::getPMValues(int *pmvalues, int len)
{
    unsigned char buf[SEN0177_DATA_LEN];

    // Expect only two elements
    if (!pmvalues || len != 2)
        return;

    if (Serial1.available())
    {
        // Read from the device
        if (Serial1.find(0x42))
        {
            Serial1.readBytes(buf, SEN0177_DATA_LEN);

            if (buf[0] == 0x4d)
            {
                if (checkValue(buf, SEN0177_DATA_LEN))
                {
                    //getPM01(buf);
                    pmvalues[0] = getPM25(buf);
                    pmvalues[1] = getPM100(buf);
                }
            }
        }
    }
    else
    {
        Serial.println("no data available");
    }

    return pmvalues;
}


/////////////////////////////////////////////////////////////////////////////
char AirPollutionMonitor::checkValue(unsigned char *buf, char len)
{
    char receiveflag = 0;
    int receiveSum = 0;

    for (int i = 0; i < (len - 2); i++)
    {
        receiveSum = receiveSum + buf[i];
    }
    receiveSum = receiveSum + 0x42;

    // Check the serial data
    if (receiveSum == ((buf[len - 2] << 8) + buf[len - 1]))
    {
        receiveSum = 0;
        receiveflag = 1;
    }
    return receiveflag;
}


/////////////////////////////////////////////////////////////////////////////
int AirPollutionMonitor::getPM10(unsigned char *buf)
{
    int val = 0;

    // PM1.0 value of the air detector module
    val = ((buf[3] << 8) + buf[4]);

    return val;
}


/////////////////////////////////////////////////////////////////////////////
int AirPollutionMonitor::getPM25(unsigned char *buf)
{
    int val = 0;

    // PM2.5 value of the air detector module
    val = ((buf[5] << 8) + buf[6]);

    return val;
}


/////////////////////////////////////////////////////////////////////////////
int AirPollutionMonitor::getPM100(unsigned char *buf)
{
    int val;

    val = ((buf[7] << 8) + buf[8]); //count PM10 value of the air detector module

    return val;
}

#endif // #ifndef AIR_POLLUTION_MONITOR__
