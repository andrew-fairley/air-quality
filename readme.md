# Description

Goal:
* Report air pollution levels
    * Replicate indoors and outdoors at several locations
* Report and Analyze
    * Publish to Google Sheets (and separately connect Tableau Public to Google Sheets)
    * If above a given threshold, publish to a Slash channel

Hardware:
* Arduino Uno Wifi Rev2
* DFRobot Gravity: Laser PM2.5 Air Quality Sensor (SEN0177)

# Building and Running

To build and run:
1. In the Arduino IDE, select Tools | Wifi101 / WifiNINA Firmware Updater
2. Update the firmware and add SSL certificates for "script.google.com" and "hooks.slack.com"
3. Clone the repo
4. Update config.h as needed
5. Create a credentials.h
```c++
String wifiCredentials[3][2] = {
  { "SSID_0", "password_0" },
  { "SSID_1", "password_1" },
  { "SSID_2", "password_2" }
};

String slackChannelPassword = "password";
String slackURLPath = "/services/x/y/z";
String gsheetsURLPath = "/macros/s/<sheet_id>/exec";
```
6. Build and run in Arduino IDE

# Open Questions
- What is the minimum cost for the device?
- How long can it be expected to run if from batteries?
